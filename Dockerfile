FROM fedora:34

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG http_proxy
ARG https_proxy

RUN set -x \
	&& dnf install -y \
		httpd \
		git \
		gitweb \
		procps-ng \
		net-tools \
		diffutils \
		rsync \
		openssh-clients \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

ARG TARGET=gitweb
COPY ${TARGET}/git.conf ${TARGET}/dav.conf /etc/httpd/conf.d/
RUN set -x \
	&& /usr/sbin/useradd -m user1
COPY ${TARGET} /home/user1/${TARGET}
RUN set -x \
	&& /usr/bin/chown -R user1:user1 /home/user1

RUN set -x \
	&& mv -v /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.org \
	&& cat /etc/httpd/conf/httpd.conf.org \
		| sed 's/^Listen 80/Listen 8080/' \
		| sed 's/^User apache/User user1/' \
		| sed 's/^Group apache/Group user1/' \
		> /etc/httpd/conf/httpd.conf \
	&& diff -C 2 /etc/httpd/conf/httpd.conf.org /etc/httpd/conf/httpd.conf \
	|| echo '/etc/httpd/conf/httpd.conf changed.'
RUN set -x \
	&& mv -v /usr/lib/tmpfiles.d/httpd.conf /usr/lib/tmpfiles.d/httpd.conf.org \
	&& cat /usr/lib/tmpfiles.d/httpd.conf.org \
		| sed 's/0 .* apache/0 user1 user1/' \
		> /usr/lib/tmpfiles.d/httpd.conf \
	&& diff -C 2 /usr/lib/tmpfiles.d/httpd.conf.org /usr/lib/tmpfiles.d/httpd.conf \
	|| echo '/usr/lib/tmpfiles.d/httpd.conf changed.'
RUN set -x \
	&& mv -v /etc/httpd/conf.d/welcome.conf /etc/httpd/conf.d/welcome.conf.bak \
	&& chown -Rv user1:user1 /var/lib/httpd \
	&& chown -Rv user1:user1 /run/httpd \
	&& chmod -Rv ugo+rwx /run/httpd
RUN set -x \
	&& cd /var/log \
	&& rmdir -v httpd \
	&& ln -sv /var/lib/pv/log httpd

RUN set -x \
	&& mv -v /etc/gitweb.conf /etc/gitweb.conf.org \
	&& cat /etc/gitweb.conf.org \
		| sed 's/^#\(our $projectroot\s*=\s*\).*/\1"\/var\/lib\/pv\/git";/' \
		> /etc/gitweb.conf \
	&& diff -C 2 /etc/gitweb.conf.org /etc/gitweb.conf \
	|| echo '/etc/gitweb.conf changed.'
RUN set -x \
	&& mv -v /etc/httpd/conf.d/gitweb.conf /etc/httpd/conf.d/0gitweb.conf.org \
	&& cat /etc/httpd/conf.d/0gitweb.conf.org \
		| sed 's/^Alias \/git/Alias \/git\/web/' \
		> /etc/httpd/conf.d/0gitweb.conf \
	&& diff -C 2 /etc/httpd/conf.d/0gitweb.conf.org /etc/httpd/conf.d/0gitweb.conf \
	|| echo '/etc/httpd/conf.d/0gitweb.conf changed.'

WORKDIR /home/user1/${TARGET}

EXPOSE 8080

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	if [ -v BACKUP_HOST ]; then
#		mkdir -v ~/.ssh
#		cp -v /var/lib/pv/id_rsa ~/.ssh
#	fi
#
#	# easy cron
#	now=`date +%s`
#	target0=$((now - now % ${CYCLE0:=86400}   + ${SCHED0:=$(( 5 * 60 +  4 * 3600 - 32400))}))
#	while [ $target0 -lt $now ]; do
#		((target0 += CYCLE0))
#	done
#
#	s=S
#	while true; do
#		pgrep -f httpd > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart httpd."
#			httpd
#		fi
#		s=Res
#
#		# easy cron
#		if [ -v BACKUP_PATH ]; then
#			if [ `date +%s` -ge $target0 ]; then
#				((target0 += CYCLE0))
#				echo "`date`: Job easy cron 1 started."
#				stamp=`date +%Y%m%d_%H%M`
#				ssh_opt='-o StrictHostKeyChecking=no -o PasswordAuthentication=no'
#				if [ -v BACKUP_HOST ]; then
#					coron=:
#					ssh_opr="ssh $ssh_opt $BACKUP_HOST"
#				fi
#				last_backup=`$ssh_opr ls $BACKUP_PATH | tail -1`
#				backup_ex0='--exclude=log'
#				if [ -z "$last_backup" ]; then
#					gen=0
#				else
#					gen=$((`$ssh_opr cat $BACKUP_PATH/$last_backup/.gen` + 1))
#					link_dest="--link-dest=../$last_backup"
#				fi
#				rsync -av --delete $backup_ex0 $BACKUP_EX -e "ssh $ssh_opt" /var/lib/pv $BACKUP_HOST$coron$BACKUP_PATH/$stamp $link_dest
#				echo $gen > .gen; scp $ssh_opt .gen $BACKUP_HOST$coron$BACKUP_PATH/$stamp
#			fi
#		fi
#		sleep 5
#	done
#
##	target0=$((now - now % ${CYCLE0:=3600}    + ${SCHED0:=$(( 0 * 60))}))									# hour
##	target0=$((now - now % ${CYCLE0:=86400}   + ${SCHED0:=$(( 0 * 60 +  0 * 3600 - 32400))}))				# day
##	target0=$((now - now % ${CYCLE0:=604800}  + ${SCHED0:=$(( 0 * 60 +  0 * 3600 + 0 * 86400 - 378000))}))	# week
##	target0=$((now - now % ${CYCLE0:=2419200} + ${SCHED0:=$(( 0 * 60 +  0 * 3600 + 0 * 86400 - 378000))}))	# 4weeks
#
##__END0__startup.sh__

USER user1
ENTRYPOINT ["bash", "startup.sh"]

